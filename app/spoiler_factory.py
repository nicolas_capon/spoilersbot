from datetime import datetime, timedelta
from typing import List
from model_cv import ImageComparator
from crawlers import CrawledObject
from model import Spoiler, Session, Set
from datetime import date, timedelta


class SpoilerFactory():

    def __init__(self, comparator: ImageComparator, lookup_days=25, use_db=True):
        self.comparator = comparator
        self.lookup_days = lookup_days
        self.use_db = use_db
        self.spoiled: List[Spoiler] = self.load_spoiled() if use_db else []

    def filter(self, crawled_objects: List[CrawledObject]) -> List[Spoiler]:
        """
        Take a list of crawled objects and determine which ones are spoilers or not
        :param crawled_objects: list of CrawledObject
        :return: list of Spoiler
        """
        local_session = Session() if self.use_db else None
        spoilers = []
        spoiled_images = [s.image for s in self.spoiled]
        # Remove duplicates within the pool of crawledObjects itself
        # because after we only test duplicate against the database
        # To verify ?
        images_set = self.comparator.remove_duplicates([c.data for c in crawled_objects])
        crawled_objects_set = [c for c in crawled_objects if c.data in images_set]
        for crawled_object in crawled_objects_set:
            # test if crawled_object data is ok and is not a duplicate vs db to create a Spoiler
            if not crawled_object.data is None and not self.comparator.is_duplicate(crawled_object.data, spoiled_images):
                if self.use_db:
                    expansion = local_session.query(Set).filter(Set.code == crawled_object.metadata.get("expansion")).first()
                else:
                    expansion = None
                spoiler = Spoiler(url=crawled_object.source,
                                  source=crawled_object.domain,
                                  found_at=crawled_object.found_at,
                                  comparator=self.comparator.method,
                                  image=crawled_object.data,
                                  set=expansion)
                spoilers.append(spoiler)
        if self.use_db:
            local_session.add_all(spoilers)
            local_session.commit()
        self.spoiled += spoilers
        return spoilers

    def flush(self) -> None:
        """Remove old spoilers from spoiled list"""
        (self.spoiled.remove(s) for s in self.spoiled if s.found_at < date.today() - timedelta(days=self.lookup_days))
    
    def load_spoiled(self) -> List[Spoiler]:
        limit_date = datetime.today() - timedelta(days=self.lookup_days)
        spoilers = Session.query(Spoiler).filter(Spoiler.found_at > limit_date).all()
        print(len(spoilers))
        # Reload descriptors
        for spoiler in spoilers:
            spoiler.image.set_descriptor(self.comparator)
        return spoilers

