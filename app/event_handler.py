from abc import ABC, abstractmethod
import config
import im_utils
from time import sleep
from telegram.ext import Updater
from typing import List
from model import Spoiler
from telegram.error import BadRequest

class Event(ABC):

    @abstractmethod
    def trigger(self, spoilers: List[Spoiler]) -> None:
        """Do something"""


class EventHandler:
    """ Handle a list of Events """
    def __init__(self, events: List[Event]):
        self.events = events

    def trigger_events(self, spoilers: List[Spoiler]) -> None:
        [event.trigger(spoilers) for event in self.events]


class TelegramEvent(Event):
    """ Send spoilers into the channel """

    def __init__(self, token: str, chat_id: int):
        self.updater = Updater(token, use_context=True)
        self.chat_id = chat_id
        self.updater.bot.send_message(chat_id=self.chat_id, text="SpoilersBot started.")

    def trigger(self, spoilers):
        for spoiler in spoilers:
            config.bot_logger.info(f"Send spoiler {spoiler} to channel.")
            set_text = ""
            if spoiler.set:
                if spoiler.source == "mythicspoiler.com":
                    set_url = f"http://mythicspoiler.com/{spoiler.set.code}/index.html"
                else:
                    set_url = f"https://scryfall.com/sets/{spoiler.set.code}"
                set_text += f"from <a href='{set_url}'>{spoiler.set.name}</a> "
            caption = f"New spoiler {set_text}!\nSource: <a href='{spoiler.url}'>{spoiler.source}</a>"
            if spoiler.image.cv_array is not None:
                # Send photo directly if image is open_cv array
                self.updater.bot.send_photo(chat_id=self.chat_id,
                                            photo=im_utils.get_file_from_cv_image(spoiler.image.cv_array),
                                            caption=caption,
                                            parse_mode="HTML")
            else:
                # Send url in message text
                try:
                    self.updater.bot.send_photo(chat_id=self.chat_id,
                                                photo=spoiler.image.location,
                                                caption=caption,
                                                parse_mode="HTML")
                except BadRequest as e:
                    config.bot_logger.error(e)

            # Avoid spam: 20 messages per minute rate
            sleep(4)
