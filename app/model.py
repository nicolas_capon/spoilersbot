import os
import pickle
import logging
import requests
import scryfall
import config
import numpy
import cv2
from datetime import datetime
from typing import Union
from sqlalchemy import Column, Integer, String, DateTime, create_engine, BLOB, orm
from sqlalchemy.orm import sessionmaker, relationship, scoped_session, reconstructor
from sqlalchemy.types import TypeDecorator, VARCHAR
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine(config.db, connect_args={'check_same_thread': False})
Base = declarative_base()

class PickleEncodedNDArray(TypeDecorator):
    "Encoder for image ndarray to the database"

    impl = VARCHAR

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = pickle.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = pickle.loads(value)
        return value


class Set(Base):

    __tablename__ = 'set'

    code = Column(String, primary_key=True)
    name = Column(String)
    released_at = Column(DateTime)
    card_count = Column(Integer)
    scryfall_id = Column(String)

    cards = relationship("Spoiler")

    def __repr__(self):
        return f"<Set(code={self.code}, name={self.name}, released_at={self.released_at}, "\
               f"card_count={self.card_count}, scryfall_id={self.scryfall_id})>"


class Image(Base):

    __tablename__ = 'image'

    id = Column(Integer, primary_key=True, autoincrement=True)
    location = Column(String)
    cv_array = Column("cv_array", PickleEncodedNDArray)

    spoiler = relationship("Spoiler", uselist=False)

    def __init__(self, location: str, comparator, cv_array=None):
        self.location = location
        self.cv_array = cv_array if cv_array is not None else self.get_cv_array()
        self.set_descriptor(comparator)

    @orm.reconstructor
    def init_on_load(self):
        # Init transient property
        self.descriptor = None

    def get_illustration(self, box=(0.5, 0.332265, 0.855655, 0.448718))-> Union[numpy.ndarray, None]:
        """Take cv card image and return portion of image containing card illustration
        Typical card box (yolo):
            center_x = 0.502232
            center_y = 0.332265
            width_% = 0.855655
            height_% = 0.448718
        """
        if self.cv_array is None:
            return
        height, width = self.cv_array.shape[:2]
        x, y, w, h = box
        x1 = int(x * width - w * width / 2)
        x2 = int(x * width + w * width / 2)
        y1 = int(y * height - h * height / 2)
        y2 = int(y * height + h * height / 2)
        return self.cv_array[y1:y2, x1:x2]

    def get_cv_array(self, flags=cv2.IMREAD_UNCHANGED) -> Union[numpy.ndarray, None]:
        """Return cv image from URL, None if url invalid"""
        if not self.location:
            return
        image = None
        try:
            with requests.get(self.location, stream=True, timeout=5) as resp:
                if resp.ok:
                    image = numpy.asarray(bytearray(resp.raw.read()), dtype="uint8")
                    image = cv2.imdecode(image, flags)
        except requests.exceptions.RequestException as e:
            logging.exception(f"[{e}] Could not get image from {self.location}")
        except cv2.error as e:
            logging.exception(f"[{e}] Could not get image from {self.location}")
        return image

    def set_descriptor(self, comparator) -> numpy.ndarray:
        """ Setter of property descriptor using comparator
        :param comparator: ImageComparator
        :return: None
        """
        self.descriptor = comparator.descript_image(image=self)

    def set_location(self, location):
        """ Setter of property location, also update cv_array """
        self.location = location
        self.cv_array = self.get_cv_array()

    def save(self, directory:str = ".", filename: str = None):
        if not filename:
            filename = str(self.id)
        cv2.imwrite(os.path.join(directory, filename + ".png"), self.cv_array)

    def __repr__(self):
        return f"<Image(id={self.id}, location={self.location})>"

    def __eq__(self, other):
        return self.location == other.location

class Spoiler(Base):

    __tablename__ = 'spoiler'

    id = Column(Integer, primary_key=True, autoincrement=True)
    found_at = Column(DateTime, default=datetime.now())
    url = Column(String)
    source = Column(String)  # Domain
    comparator = Column(String)

    image_id = Column(Integer, ForeignKey("image.id"))
    image: Image = relationship(Image, uselist=False)

    set_code = Column(String, ForeignKey("set.code"))
    set: Set = relationship(Set, uselist=False)

    def __repr__(self):
        return f"<Spoiler(id={self.id}, found_at={self.found_at}, url={self.url}, comparator={self.comparator}, "\
               f"source={self.source}, image_id={self.image_id}, set_code={self.set_code})>"


def import_scryfall_set(s, digital=None):
    local_session = Session()
    """Import scryfall Set to db, set digital to None to import all Sets"""
    if digital is None or s["digital"] == digital:
        local_session.add(Set(code=s["code"],
                              name=s["name"],
                              released_at=scryfall.to_datetime(s["released_at"]),
                              card_count=s["card_count"],
                              scryfall_id=s["id"]))
        return True


def update_sets():
    local_session = Session()
    sets_ref = [s.code for s in local_session.query(Set.code)]
    sets = [s for s in scryfall.get_set_list() if s["code"] not in sets_ref and not s["digital"]]
    s_cpt = 0
    for s in sets:
        if import_scryfall_set(s):
            s_cpt += 1
    local_session.commit()

Base.metadata.create_all(engine)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)

