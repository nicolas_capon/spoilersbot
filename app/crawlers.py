from __future__ import annotations
import os
import config
import scryfall
import requests
from datetime import datetime, timedelta
from typing import List
from model import Image
from model_cv import ImageComparator
from abc import ABC, abstractmethod
from reddit import praw_request as reddit_request, Reddit
from mythicspoiler import MythicSpoiler
from yolo import Yolo

VALID_EXTENSIONS = [".jpg", ".jpeg", ".png"]


class CrawledObject(ABC):
    pass


class Crawler(ABC):

    @abstractmethod
    def crawl(self) -> List[CrawledObject]:
        """
        Get a list of CrawledCard
        """
        pass

    @abstractmethod
    def create_crawled_card_image(self) -> List[CrawledCardImage]:
        """
        Get a list of CrawledCardImage
        """
        pass


class CrawledCardImage(CrawledObject):

    def __init__(
            self,
            source: str,
            domain: str,
            image: Image,
            expansion: str = "",
            found_at: datetime = datetime.now(),
    ):
        self.source = source
        self.domain = domain
        self.found_at = found_at
        self.data = image
        self.metadata = {"expansion": expansion}

    def __eq__(self, other):
        """Compare only the image url"""
        return self.data == other.data

    def __repr__(self) -> str:
        return (
            f"<CrawledCardImage(source={self.source}, domain={self.domain},"
            f"found_at={self.found_at}, data={self.data}, metadata={self.metadata})>"
        )


class ScryfallCrawler(Crawler):

    domain = "scryfall.com"

    def __init__(self,
                 comparator: ImageComparator,
                 crawledCards: List[str] = []):
        self.crawledCards = crawledCards
        self.comparator = comparator

    def crawl(self):
        cards = scryfall.get_futur_cards(datetime.now() -
                                         timedelta(config.limit))
        news = []
        for card in cards:
            card_url = scryfall.get_card_url(card)
            if not card_url in self.crawledCards:
                crawled_cards_im = self.create_crawled_card_image(card)
                # Add cards to the already crawled list of cards
                news += crawled_cards_im
                self.crawledCards.append(card_url)
        return news

    def create_crawled_card_image(self, card) -> List[CrawledCardImage]:
        """Create CrawledCardImage for each side of the card if any"""
        cards = []
        card_url = scryfall.get_card_url(card)
        for image_url in scryfall.get_image_urls(card):
            cards.append(
                CrawledCardImage(
                    source=card_url,
                    domain=self.domain,
                    image=Image(image_url, self.comparator),
                    expansion=card.get("set"),
                ))
        return cards


class MythicSpoilerCrawler(Crawler):

    domain = "mythicspoiler.com"

    def __init__(self,
                 comparator: ImageComparator,
                 crawledCards: List[str] = []):
        self.ms = MythicSpoiler()
        self.crawledCards = crawledCards
        self.comparator = comparator

    def crawl(self):
        cards = self.ms.get_cards_from_news()
        news = []
        for page, image_url, card_set in cards:
            if not page in self.crawledCards:
                # Add cards to the already crawled list of cards
                news += self.create_crawled_card_image(page, image_url,
                                                       card_set)
                self.crawledCards.append(page)
        return news

    def create_crawled_card_image(self, page: str, image_url: str,
                                  card_set: str) -> List[CrawledCardImage]:
        return [
            CrawledCardImage(
                source=page,
                image=Image(image_url, self.comparator),
                expansion=card_set,
                domain=self.domain,
            )
        ]


class RedditCrawler(Crawler):

    domain = "reddit.com"

    def __init__(self,
                 comparator: ImageComparator,
                 crawled_submissions: List[str] = []):
        self.reddit = Reddit(subreddit="magicTCG")
        self.crawled_submissions: List[str] = crawled_submissions
        self.comparator = comparator
        self.yolo = Yolo(config.model, config.classes, config.conf)
        self._accepted_mime = ["image/png", "image/jpeg", "image/gif"]

    @reddit_request
    def crawl(self):
        news = []
        for submission in self.reddit.browse_news():
            source = "https://www." + self.domain + submission.permalink
            # Take only image_url not already crawled or not marked as spoiler by subreddit
            if not source in self.crawled_submissions and self.reddit.is_spoiler(
                    submission):
                news += self.create_crawled_card_image(submission)
                self.crawled_submissions.append(source)
        return news

    def create_crawled_card_image(self, submission) -> List[CrawledCardImage]:
        cards = []
        source = "https://www." + self.domain + submission.permalink
        # Gather all images url in this list to later run yolo on it
        images_url_for_yolo = []
        set_code = self.reddit.detect_set(submission.title)
        if hasattr(submission, "is_gallery"):
            for _, value in submission.media_metadata.items():
                images_url_for_yolo.append(value.get("s", {}).get("u"))

        elif hasattr(submission, "preview"):
            # If submission contains a image
            images_url_for_yolo.append(
                submission.preview.get("images",
                                       [])[0].get("source").get("url"))

        elif hasattr(submission, "selftext") and not submission.selftext:
            # If submission is not only text and media has correct mimetype
            r = requests.get(submission.url)
            mime = r.headers["Content-Type"]
            if mime in self._accepted_mime:
                images_url_for_yolo.append(submission.url)

        for image_url in images_url_for_yolo:
            # For each image run yolo to see if this image contains multiple cards on it
            subspoilers = self.yolo.get_detected_objects(image_url)
            for image, _ in subspoilers:
                # For each card image on main image, create a crawledCard object
                cards.append(
                    CrawledCardImage(
                        source=source,
                        image=Image(image_url, self.comparator,
                                    cv_array=image),
                        expansion=set_code,
                        domain=self.domain,
                    ))
        return cards


class LocalCrawler(Crawler):
    """Crawl images from a local directory"""

    domain = "local"

    def __init__(
        self,
        comparator: ImageComparator,
        directory: str = "",
        crawledCards: List[str] = [],
    ):
        self.directory = directory
        self.crawledCards = crawledCards
        self.comparator = comparator
        if not directory:
            self.directory = os.path.join(os.path.dirname(__file__),
                                          "test/images")
        from cv2 import imread

        self._read = imread

    def crawl(self):
        news = []
        for card in self.get_im_path(self.directory):
            if not card in self.crawledCards:
                # Add cards to the already crawled list of cards
                news.append(self.create_crawled_card_image(card))
                self.crawledCards.append(card)
        return news

    def create_crawled_card_image(self, path: str) -> List[CrawledCardImage]:
        return CrawledCardImage(
            source=path,
            image=Image(path, self.comparator, cv_array=self._read(path)),
            expansion=None,
            domain=self.domain,
        )
        

    @staticmethod
    def get_im_path(directory: str) -> List[str]:
        return [
            os.path.join(directory, f) for f in os.listdir(directory)
            if os.path.splitext(f)[1] in VALID_EXTENSIONS
        ]
