import os
import re
import pytesseract
import PIL
from tqdm import tqdm
import textdistance
from PIL import Image, ImageFilter
from typing import Any, Union, Tuple, List

tesseract_path = r'C:\\Users\\ncapon\\Tesseract-OCR\\tesseract.exe'
pytesseract.pytesseract.tesseract_cmd = tesseract_path

TESSERACT_DETECTION_CONFIG = "--psm 3 -l eng"

Descriptor = Any
Distance = Union[int, float]
Comparable = Tuple[Distance, Any]
Album = List[Comparable]

def process_image(path: str) -> str:
    img = Image.open(path).convert("L")
    w, h = img.size
    l = w / 15
    t = h / 25
    r = w / 1.3
    b = h / 10
    img_crop = img.crop((l, t, r, b))
    img_thresh = threshold(img_crop, thresh = 130)
    text = pytesseract.image_to_string(img_thresh, lang="eng")# , lang=None, config=TESSERACT_DETECTION_CONFIG)
    return text

def threshold(img, thresh : int = 200):
    fn = lambda x : 255 if x > thresh else 0
    return img.convert('L').point(fn, mode='1')

class NameComparator:

    method = "textdistance"

    def __init__(self, threshold: float = 0.7):
        self._method = textdistance.jaro_winkler
        self._threshold = threshold

    def descript(self, path):
        img = Image.open(path).convert("L")
        w, h = img.size
        l = w / 15
        t = h / 25
        r = w / 1.3
        b = h / 10
        img_crop = img.crop((l, t, r, b))
        img_thresh = threshold(img_crop, thresh = 130)
        text = pytesseract.image_to_string(img_thresh, lang="eng")# , lang=None, config=TESSERACT_DETECTION_CONFIG)
        # Remove all non alphabetical characters
        regex = re.compile('[^a-zA-Z]')
        return regex.sub("", text).lower()
    
    def get_closest_matches(self, ref, objects):
        scores = [(self._method(o[0], ref[0]), o[1], o[0]) for o in objects if len(o[0]) > 3]
        return sorted(scores, key=lambda x: x[0], reverse=True)

    def is_duplicate(self, ref, objects):
        closest = self.get_closest_matches(ref, objects)[0]
        return closest[0] >= self._threshold

    def remove_duplicates(self, objects, confidence):
        unique_album = []
        for _ in range(len(album)):
            image = album.pop()
            if not self.is_duplicate(image, album):
                unique_album.append(image)
        return unique_album

def test_comparator(comp):
    ref_dir = "C:/Users/ncapon/spoilersbot/app/test/images"
    dup_dir = "C:/Users/ncapon/spoilersbot/app/test/duplicates/image"
    refs: List[Comparable] = []
    dups: List[Comparable] = []
    for img_path in tqdm(os.listdir(ref_dir), position=0, desc="Compute refs descriptors"):
        description = comp.descript(os.path.join(ref_dir, img_path))
        refs.append((description, img_path))
    for img_path in tqdm(os.listdir(dup_dir), position=0, desc="Compute duplicates descriptors"):
        description = comp.descript(os.path.join(dup_dir, img_path))
#         if description:
        dups.append((description, img_path))

    cpt = 0
    for ref in tqdm(refs, position=0, desc="Test comparator efficiency"):
        if not ref[0]:
            continue
        elif comp.is_duplicate(ref, dups):
            cpt += 1

    print(comp.method, "detection rate =", cpt / len(refs))

if __name__ == "__main__":
    test_comparator(NameComparator())
    # 
    #     img_dict = {"1": "Ob Nixilis, the Adversary", "2": "Jon Irenicus, Shattered One", "3": "Angelic Sleuth", "3_2": "Angelic Sleuth",
    #             "4": "Gavel of the Righteous", "5": "Aven Courier", "5_2": "Aven Courier", "6": "Tivit, Seller of Secrets", "7": "Flawless Forgery", "8": "Agent's Toolkit",
    #             "9": "Obscura Confluence", "10": "Bribe Taker", "10_2": "Bribe Taker", "11": "Crash the Party", "12": "Boss's Chauffeur", "13": "Change of Plans",
    #             "14": "Forgeborn Phoenix", "15": "Forceful Cultivator", "16": "Better Offer", "17": "Kami of Mourning", "18": "Kami of Bamboo Groves", "19": "Plains"}
    #     directory = "C:/Users/ncapon/spoilersbot/app/test/duplicates/image"
    #     cpt = 0
    #     for img_path in tqdm(os.listdir(directory), position=0):
    #         path = os.path.join(directory, img_path)
    #         # if img_path == "3.jpg":
    #         text = process_image(path).strip()
    #         cardname = img_dict[os.path.splitext(img_path)[0]]
    #         score = None
    #         # For further infos on methods see : https://theautomatic.net/2019/11/13/guide-to-fuzzy-matching-with-python/
    #         method = textdistance.jaro_winkler
    #         if text:
    #             score = method(text, cardname)
    #         for key, value in img_dict.items():
    #             if method(text, value) > 0.7:
    #                 if value == cardname:
    #                     cpt += 1
    #                 tqdm.write(" ".join(["Found simlarity between", cardname, "and", value]))
    #         tqdm.write(" ".join([img_path, cardname, "// OCR:", text, "--score=", str(score)]))
    # 
    #     print("dectection rate =", cpt / len(os.listdir(directory)))
