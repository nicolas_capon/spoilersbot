import config
import praw
import re


def praw_request(func):
    """Catch praw exceptions to logger"""
    def wrap(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except (praw.exceptions.PRAWException, praw.exceptions.RequestException) as e:
            logging.exception(e)
            result = None
        return result
    return func


class Reddit:
    search_limit = 20
    stream = False
    reddit_set_reg = r"^\[(.{3,4})\]"
    flairs = ["Spoiler", "News"]
    domain = "self.magicTCG"

    @praw_request
    def __init__(self, subreddit=None, read_only=True):
        self.reddit = praw.Reddit(client_id=config.client_id,
                                  client_secret=config.client_secret,
                                  password=config.password,
                                  user_agent=config.user_agent,
                                  username=config.username)
        self.reddit.read_only = read_only
        self.subreddit = subreddit
        if subreddit:
            self.subreddit = self.reddit.subreddit(subreddit)

    @praw_request
    def get_submission(self, submission_id: str)-> praw.models.Submission:
        return self.reddit.submission(submission_id)

    @praw_request
    def search_spoilers(self):
        return self.subreddit.search('flair:"spoiler"', limit=self.search_limit)

    @praw_request
    def browse_news(self, func, *args, **kwargs):
        return self.reddit.subreddit("magicTCG").new()

    @praw_request
    def browse_hots(self, func, *args, **kwargs):
        for submission in self.reddit.subreddit("magicTCG").hot():
            func(submission, *args, **kwargs)

    @praw_request
    def stream_subreddit(self, func, *args, **kwargs):
        if self.subreddit:
            for submission in self.subreddit.stream.submissions():
                func(submission, *args, **kwargs)

    def is_spoiler(self, submission):
        """
        Test if a reddit submission can be considered as a spoiler
        :param submission: PRAW submission object
        :return: True if submission is spoiler, False if not
        """
        # Discard any submission containing no external link to a potential spoiler
        if submission.domain == self.domain:
            return False
        # If submission is flaired or tag as spoiler:
        elif submission.spoiler or submission.link_flair_text in self.flairs:
            if submission.link_flair_text == "News" and not self.detect_set(submission.title):
                # If submission is a news and dont comport the set code in between brackets, rejects it
                return False
            log = f"Reddit spoiler detected (flair={submission.link_flair_text}): [{submission.id}] {submission.title}"
            config.bot_logger.info(log)
            return True
        else:
            return False

    def detect_set(self, text: str):
        """
        Test if text contain a set_code usually in between brackets at beginning of string
        :param text: String text to be tested
        :return: set_code (3 or 4 letters in lower_case)
        """
        reg = re.compile(self.reddit_set_reg)
        match = reg.findall(text)
        if match:
            return match[0].lower()
