from time import process_time, sleep
import config
from model import update_sets
from crawlers import ScryfallCrawler, MythicSpoilerCrawler, RedditCrawler
from spoiler_factory import SpoilerFactory
from model_cv import OrbComparator
from event_handler import EventHandler, TelegramEvent
from model import Session, Spoiler

def startbot():
    update_sets()
    comparator = OrbComparator()
    crawlers = [ScryfallCrawler(comparator, [s.url for s in Session().query(Spoiler.url).filter(Spoiler.source == ScryfallCrawler.domain).all()]),
                MythicSpoilerCrawler(comparator, [s.url for s in Session().query(Spoiler.url).filter(Spoiler.source == MythicSpoilerCrawler.domain).all()]),
                RedditCrawler(comparator, [s.url for s in Session().query(Spoiler.url).filter(Spoiler.source == RedditCrawler.domain).all()])]
    
    factory = SpoilerFactory(comparator=comparator)
    events = [TelegramEvent(token=config.telegram_token, chat_id=config.chat_id)]
    spoilers_events_handler = EventHandler(events)

    while True:
        start = process_time()
        factory.flush()
        crawled_objects = []
        for c in crawlers:
            cards = c.crawl()
            crawled_objects += cards
        spoilers = factory.filter(crawled_objects)
        spoilers_events_handler.trigger_events(spoilers)
        duration = process_time() - start
        update_sets()
        # Ensure that a crawl is done at a minimum frequency
        if duration < config.crawl_frequency:
            sleep(config.crawl_frequency - duration)


if __name__ == "__main__":
    startbot()
