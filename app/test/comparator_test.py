import sys
[sys.path.append(i) for i in ['.', '..']]
import os
from cv2 import imread
from model_cv import ImageComparator, OrbComparator, HistogramComparator, HashComparator, NameComparator
from model import Image

MIN_DETECTION_RATE = 90

def is_strong_comparator(comparator: ImageComparator,
                         dup_path: str="image",
                         verbose:bool = True) -> bool:
    # Load images
    images = []
    im_dir = os.path.join(os.path.dirname(__file__), "images")
    for file in os.listdir(im_dir):
        if file.endswith(".jpg"):
            im_path = os.path.join(im_dir, file)
            im = Image(im_path, comparator=comparator, cv_array=imread(im_path))
            images.append(im)
    # Load duplicates
    duplicates = []
    for file in os.listdir(f"{im_dir}/duplicates/{dup_path}/"):
        if file.endswith(".jpg"):
            im_path = os.path.join(f"{im_dir}/duplicates/{dup_path}/", file)
            im = Image(im_path, comparator=comparator, cv_array=imread(im_path))
            duplicates.append(im)

    # Calculate success rate
    r = [comparator.is_duplicate(d, images) for d in duplicates]
    detection_rate = round(r.count(True)/len(r) * 100, 2)
    if verbose:
        print(f"[{comparator.method}] Taux de détection = {detection_rate} %")
    return detection_rate >= MIN_DETECTION_RATE


def test_ORB():
    assert is_strong_comparator(OrbComparator())

# def test_Hist():
#     assert is_strong_comparator(HistogramComparator())
# 
# def test_Hash():
#     assert is_strong_comparator(HashComparator())
# 
# def test_name():
#     assert is_strong_comparator(NameComparator(), dup_path="text")
# 
