import sys
[sys.path.append(i) for i in ['.', '..']]
from spoiler_factory import SpoilerFactory
from crawlers import LocalCrawler, MythicSpoilerCrawler, RedditCrawler, ScryfallCrawler
from reddit import Reddit
from scryfall import get_card_by_id
import model_cv

COMPARATOR = model_cv.OrbComparator()

def test_factory_filter_local_crawler():
    factory = SpoilerFactory(COMPARATOR, use_db=False)
    crawler = LocalCrawler(COMPARATOR)
    cards = crawler.crawl()
    factory.filter(cards)
    factory.flush()
    assert len(factory.filter(cards)) == 0

def test_factory_filter_multiple_crawlers():
    factory = SpoilerFactory(COMPARATOR, use_db=False)
    # Load lot of local images
    factory.filter(LocalCrawler(COMPARATOR).crawl())
    # fetch web images who have a local duplicate and test duplicate filter
    ms_cards = [("http://mythicspoiler.com/clb/cards/seasoneddungeoneer.html", "http://mythicspoiler.com/clb/cards/seasoneddungeoneer.jpg", "clb"),
                ("https://mythicspoiler.com/clb/cards/renarimerchantofmarvels.html", "https://mythicspoiler.com/clb/cards/renarimerchantofmarvels.jpg", "clb")]
    # Remaining cards after duplicate filter (should be zero)
    ms_remaining_num = len(factory.filter([MythicSpoilerCrawler(COMPARATOR).create_crawled_card_image(page=p, image_url=i, card_set=c)[0] for p, i, c in ms_cards]))
    re_cards = [Reddit().get_submission("v29k01"), Reddit().get_submission("uzong5"), Reddit().get_submission("uukpzi")]
    re_remaining_num = len(factory.filter([RedditCrawler(COMPARATOR).create_crawled_card_image(c)[0] for c in re_cards]))
    sc_cards = ["7fee3f76-20a8-4621-84fb-ddf79c955532", "648d9da3-5790-4d64-8b52-83141d2faf36", "80290716-17c3-41e4-8ef8-814dd25b11e4"]
    sc_remaining_num = len(factory.filter([ScryfallCrawler(COMPARATOR).create_crawled_card_image(get_card_by_id(c))[0] for c in sc_cards]))
    # Test if all are equal to zero
    print([sc_remaining_num, re_remaining_num, ms_remaining_num])
    assert not any([sc_remaining_num, re_remaining_num, ms_remaining_num])

# def test_factory_flush():
#     # TODO
#     assert False

if __name__ == "__main__":
    test_factory_filter_multiple_crawlers()
