import os
import sys
[sys.path.append(i) for i in ['.', '..']]
import crawlers
import model_cv
from model import Image
from numpy import ndarray

COMPARATOR = model_cv.OrbComparator()

def is_valid_crawled_card_image(o):
    return isinstance(o, crawlers.CrawledCardImage) and isinstance(o.data, Image) and isinstance(o.data.cv_array, ndarray)

def test_local_crawler():
    directory = os.path.join(os.path.dirname(__file__), "images")
    crawler = crawlers.LocalCrawler(comparator=COMPARATOR, directory=directory)
    cards = crawler.crawl()
    assert cards and all([is_valid_crawled_card_image(c) for c in cards])

def test_mythic_spoiler_crawler():
    crawler = crawlers.MythicSpoilerCrawler(comparator=COMPARATOR)
    cards = crawler.crawl()
    assert cards and all([is_valid_crawled_card_image(c) for c in cards])

def test_scryfall_crawler():
    crawler = crawlers.ScryfallCrawler(comparator=COMPARATOR)
    cards = crawler.crawl()
    assert cards and all([is_valid_crawled_card_image(c) for c in cards])

# def test_reddit_crawler():
#     # TODO
#     assert False
