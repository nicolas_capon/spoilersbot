from __future__ import annotations
from typing import List, Any, Union, Tuple
from numpy import ndarray, frombuffer
from abc import ABC, abstractmethod
from cv2 import ORB_create, BFMatcher, calcHist, compareHist, normalize, HISTCMP_BHATTACHARYYA
from distance import hamming
from PIL import Image as PILImage
from imagehash import phash
from model import Image
# import pickle
import config
import cv2
# TODO: add pytesseract and textdistance to requirements.txt
# import textdistance
# import pytesseract
import re

Descriptor = Any
Distance = Union[int, float]
Album = List[Image]


class ImageComparator(ABC):

    @property
    def method(self) -> str:
        """
        Method used to compare images
        :return: str
        """
        pass

    @abstractmethod
    def descript_image(self, image: Image) -> Descriptor:
        """
        Descript the image with given method
        :param image: Image object
        :return: a description of the image
        """
        pass

    @abstractmethod
    def get_closest_matches(self, ref: Image, album: Album, limit: int = 1) -> List[Tuple[Distance, Image]]:
        """
        Get the closest album images from given Image ref
        :param ref: Image to search
        :param album: Image to compare against ref
        :param limit: int length of results to be returned
        :return: List[(distance, Image)] list of tuple containing the distance between this image and the reference
        """
        pass

    @abstractmethod
    def is_duplicate(self, ref: Image, album: Album) -> bool:
        """
        Check for duplicate of the ref Image in list of Images using a tolerance threshold
        :param ref: Image to search
        :param album: Image to compare against ref
        :return: bool True if ref image considered in the album, False if not
        """
        pass
    
    @abstractmethod
    def remove_duplicates(self, album: Album) -> Album:
        """
        Compare image descriptors between them to remove potential duplicates
        :param album: list of Image model object
        :return: initial list minus duplicates
        """
        pass


class OrbComparator(ImageComparator):

    method = "ORB"

    def __init__(self, lowe_ratio: float = 0.6, threshold: int = 10):
        self._finder = ORB_create()
        self._matcher = BFMatcher()
        self._lowe_ratio = lowe_ratio
        self._threshold = threshold

    def descript_image(self, image: Image) -> Union[ndarray, None]:
        illustration = image.get_illustration()
        if not illustration is None:
#            return pickle.dumps(self._finder.detectAndCompute(illustration, None))#[1])
            return self._finder.detectAndCompute(illustration, None)[1]

    def get_closest_matches(self, ref: Image, album: Album) -> List[Tuple[Distance, Image]]:
        matches = []
        queryDescriptors = ref.descriptor
        for img in album:
            if not hasattr(img, "descriptor"):
                continue
            elif img.descriptor is None:
                continue
            else:
                trainDescriptors = img.descriptor
            try:
                # poi is a list of potential match
                poi = self._matcher.knnMatch(queryDescriptors, trainDescriptors, k=2)
            except cv2.error as e:
                poi = []
                config.bot_logger.exception(f"[{e}] - [{ref.location}]queryDescriptors: {queryDescriptors}\ntrainDescriptors: {trainDescriptors}")
                continue
            good_matches = []
            for i in range(0, len(poi)):
                if len(poi[i]) == 1:
                    # Case of an exact match
                    m = poi[i][0]
                    good_matches.append([m])
                    return [(9999, img)]
                elif len(poi[i]) > 1:
                    # Case of multiple matches
                    m, n = poi[i]
                    if m.distance < self._lowe_ratio * n.distance:
                        good_matches.append([m])

            matches.append((len(good_matches), img))
        return sorted(matches, reverse=True, key=lambda x: x[0])

    def is_duplicate(self, ref: Image, album: Album) -> bool:
        # Test first for exact match
        # a bit redundant with get_closest_match in case of exact match
        # but can avoid to go in this function, saving time...
        comp = []
        for img in album:
            # Compare ndarrays
            if ref.descriptor.size == img.descriptor.size:
                eq = ref.descriptor == img.descriptor
                comp.append(eq.all())
            else:
                comp.append(False)

        if any(comp):
            return True
        
        # Then we test for fuzzy matching
        if album and (m := self.get_closest_matches(ref, album)):
            return m[0][0] >= self._threshold
        else:
            return False

    def remove_duplicates(self, album: Album) -> Album:
        unique_album = []
        for _ in range(len(album)):
            image = album.pop()
            if not self.is_duplicate(image, album):
                unique_album.append(image)
        return unique_album


class HistogramComparator(ImageComparator):

    method = "Histogram"

    def __init__(self, threshold: float = 29):
        self.comp = HISTCMP_BHATTACHARYYA
        self._threshold = threshold

    def descript_image(self, image: Image):
        hist = calcHist([image.get_illustration()], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
        return normalize(hist, hist).flatten()

    def get_closest_matches(self, ref: Image, album: Album, limit=1):
#        distances = sorted([(compareHist(ref.descriptor,
#                                         frombuffer(img, dtype=ref.descriptor.dtype),
#                                         self.comp) * 100,
#                             frombuffer(img, dtype=ref.descriptor.dtype))
#                            for img in album],
#                           key=lambda x: x[0])[:limit]
        distances = sorted([(compareHist(ref.descriptor,
                                         img.descriptor,
                                         self.comp) * 100,
                             img.descriptor)
                            for img in album],
                           key=lambda x: x[0])[:limit]
        return distances

    def is_duplicate(self, ref: Image, album: Album) -> bool:
        result = self.get_closest_matches(ref, album)[0][0]
        return result <= self._threshold

    def remove_duplicates(self, album: Album, confidence) -> Album:
        copy_album = album
        for n, image in enumerate(album):
            descriptors = [i.descr for i in copy_album]
            for d, h in self.get_closest_matches(image, descriptors):
                if d < confidence and not d == 0:
                    del copy_album[n]
                    break
        return copy_album


class HashComparator(ImageComparator):

    method = "Perceptual Hashing"

    def __init__(self, hash_size: int = 16, threshold: float = 45):
        self._hash_size = hash_size
        self._threshold = threshold

    def descript_image(self, image):
        return str(phash(PILImage.fromarray(image.get_illustration()), hash_size=self._hash_size))

    def get_closest_matches(self, ref: Image, album: Album, limit=1):
        return sorted([(hamming(ref.descriptor, i.descriptor), i) for i in album], key=lambda x: x[0])[:limit]

    def is_duplicate(self, ref: Image, album: Album) -> bool:
        closest = self.get_closest_matches(ref, album)[0][0]
        return closest <= self._threshold

    def remove_duplicates(self, album: Album, confidence) -> Album:
        copy_album = album
        for n, image in enumerate(album):
            descriptors = [i.descriptor for i in copy_album]
            for d, h in self.get_closest_matches(image, descriptors):
                if d < confidence and not d == 0:
                    del copy_album[n]
                    break
        return copy_album


class NameComparator:

    method = "OCR"

    def __init__(self, threshold: float = 0.7):
        self._method = textdistance.jaro_winkler
        self._threshold = threshold

    def descript_image(self, image: Image):
        img = PILImage.open(image.location).convert("L")
        w, h = img.size
        l = w / 15
        t = h / 25
        r = w / 1.3
        b = h / 10
        img_crop = img.crop((l, t, r, b))

        # Convert to black and white
        fn = lambda x : 255 if x > self._threshold else 0
        img_thresh = img_crop.convert('L').point(fn, mode='1')
        # Extract text
        text = pytesseract.image_to_string(img_thresh, lang="eng")# , lang=None, config=TESSERACT_DETECTION_CONFIG)
        # Remove all non alphabetical characters
        regex = re.compile('[^a-zA-Z]')
        return regex.sub("", text).lower()
    
    def get_closest_matches(self, ref: Image, album: Album):
        scores = [(self._method(i.descriptor, ref.descriptor), i)
            for i in album if len(i.descriptor) > 3]
        return sorted(scores, key=lambda x: x[0], reverse=True)

    def is_duplicate(self, ref: Image, album: Album):
        closest = self.get_closest_matches(ref, album)
        if closest:
            return closest[0][0] >= self._threshold
        else:
            return False

    def remove_duplicates(self, album):
        unique_album = []
        for _ in range(len(album)):
            image = album.pop()
            if not self.is_duplicate(image, album):
                unique_album.append(image)
        return unique_album
