# set base image (host OS)
FROM python:3.8

# set the working directory in the container
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# copy the dependencies file to the working directory
COPY essential-requirements.txt /usr/src/
COPY requirements.txt /usr/src/

# install dependencies
RUN pip install --upgrade pip
RUN apt-get update
# If you get this error:
# ImportError: libGL.so.1: cannot open shared object file: No such file or directory 
# Run the following command first
RUN apt-get install ffmpeg libsm6 libxext6  -y
# First stage install large dependencies
RUN pip install --no-cache-dir -r /usr/src/essential-requirements.txt
# Second stage
RUN pip install --no-cache-dir -r /usr/src/requirements.txt

# copy the content of the local src directory to the working directory
# COPY ./app /usr/src/app

# command to run on container start
CMD [ "python", "./main.py" ]
